# terraform-provider-libvirt

Terraform provider to provision infrastructure with Linux's KVM using libvirt

## github repo

You can find the source of this project on github

<https://github.com/dmacvicar/terraform-provider-libvirt>

## Download provider

Download terraform libvirt provider binary from here:

[gitlab:terraform-provider-libvirt](https://gitlab.com/terraform-provider/terraform-provider-libvirt/-/jobs/artifacts/master/browse?job=run-build)

## terraform v1.x

eg.

```bash
VERSION=0.8.3

# Create Local directory
mkdir -pv ~/.local/share/terraform/plugins/registry.terraform.io/dmacvicar/libvirt/${VERSION}/linux_amd64/
cd        ~/.local/share/terraform/plugins/registry.terraform.io/dmacvicar/libvirt/${VERSION}/linux_amd64/

# Download latest version
curl -sLo terraform-provider-libvirt https://gitlab.com/terraform-provider/terraform-provider-libvirt/-/jobs/artifacts/master/raw/terraform-provider-libvirt?job=run-build

# Make it executable
chmod +x terraform-provider-libvirt

```

### Provider.tf

```yaml
terraform {
  required_providers {
    libvirt = {
      source = "dmacvicar/libvirt"
    }
  }
}

provider "libvirt" {
  # Configuration options
}

```

## Initialize your terraform project

```bash
terraform init

```
